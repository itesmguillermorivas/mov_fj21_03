package mx.itesm.firebaseexample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private DatabaseReference dbRef;
    private EditText login, password, value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = findViewById(R.id.login);
        password = findViewById(R.id.password);
        value = findViewById(R.id.value);

        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    protected void onStart() {
        super.onStart();

        // test to see if user is logged in
        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null)
        {
            // do something if its logged in (or not!)
        }

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        dbRef = db.getReference("obj/variable2");

        // since the database is realtime
        // we listen for a change
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String value = snapshot.getValue(String.class);
                Toast.makeText(MainActivity.this, value, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // create a new user with the values given on the GUI
    public void saveUser(View v){

        mAuth.createUserWithEmailAndPassword(login.getText().toString(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            // do something on success
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(
                                    MainActivity.this,
                                    "USER: " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {

                            // do something on failure
                            Toast.makeText(
                                    MainActivity.this,
                                    "FAILURE: " + task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void login(View v){

        // logout to do login again
        mAuth.signOut();

        mAuth.signInWithEmailAndPassword(login.getText().toString(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            // do something on success
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(
                                    MainActivity.this,
                                    "USER: " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();

                            // how to retrieve info from the user

                            // check if user is logged in
                            if(user != null){

                                String name = user.getDisplayName();
                                String email = user.getEmail();
                                Uri photoUrl = user.getPhotoUrl();

                                boolean verified = user.isEmailVerified();

                                String uid = user.getUid();
                                // String token = user.getIdToken();
                                Toast.makeText(MainActivity.this, "USER IS LOGGED IN: " + uid, Toast.LENGTH_SHORT).show();
                            }
                        }else{

                            // do something on failure
                            Toast.makeText(
                                    MainActivity.this,
                                    "FAILURE: " + task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }
    public void saveValue(View v){

        Toast.makeText(this, "SAVING VALUE", Toast.LENGTH_SHORT).show();
        dbRef.setValue(value.getText().toString());
    }
}